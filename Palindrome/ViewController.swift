//
//  ViewController.swift
//  Palindrome
//
//  Created by Andres Pineda on 7/20/17.
//  Copyright © 2017 pinedax. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var inputTextField: UITextField!
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBAction func runProgramTapped(_ sender: Any) {
        resultLabel.text = ""
        
        if let input = self.inputTextField.text, input.characters.count > 0 {
            let palindrome = isPalindrome(input: input)
            resultLabel.text = palindrome ? "True" : "False"
        }
    }
    
    func isPalindrome(input: String) -> Bool {
        var localInput = input
        
        let inputLength = localInput.characters.count
        
        if inputLength == 0 {
            return false
        }
        
        if inputLength == 1 {
            return true
        }
        
        let first = localInput.characters.popFirst()
        let last = localInput.characters.popLast()
        
        if first != last {
            return false
        }
        
        if localInput.characters.count > 0 {
            return isPalindrome(input: localInput)
        }
        
        return true
    }

}

